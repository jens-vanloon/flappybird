﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        MainWindow main = new MainWindow();
        public Window1()
        {
            InitializeComponent();
            button2.Visibility = Visibility.Hidden;
            button1.Visibility = Visibility.Hidden;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            /*var StartX = Canvas.GetLeft(RFID_Reader);*/
            var StartY = Canvas.GetTop(RFID_Reader);

            /*var EndX = RFID_Reader.Width / 2 + Canvas.GetLeft(RFID_Reader) - StartX - (RFID_Reader.Width / 2);*/
            var EndY = RFID_Reader.Height / 2 + Canvas.GetTop(RFID_Reader) + StartY + (RFID_Reader.Height * 2);

            /*var AnimationX = new DoubleAnimation(0, EndX, TimeSpan.FromSeconds(1));*/
            var AnimationY = new DoubleAnimation(0, EndY, TimeSpan.FromSeconds(2));

            var Transform = new TranslateTransform();
            RFID_Token_Canvas.RenderTransform = Transform;

            /*Transform.BeginAnimation(TranslateTransform.XProperty, AnimationX);*/
            Transform.BeginAnimation(TranslateTransform.YProperty, AnimationY);
        }
        private void Rectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            button1.Visibility = Visibility.Visible;
            button2.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            main.Show();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            main.Close();
        }

        private void menu_MouseEnter(object sender, MouseEventArgs e)
        {
            button1.Visibility = Visibility.Visible;
            button2.Visibility = Visibility.Visible;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
