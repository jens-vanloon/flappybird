# WPF - Flappy Bird
============

## Het Concept

Remake van de bekende game "Flappy Bird" doormiddel van C#.

## Theorie achter de code
1. De vogel word elke keer naar beneden getrokken door een timer gebonden aan -ypos
2. Wanneer men op een button klikt (Verbonden met een 'case' en onzichtbaar gemaakt), wordt de vogel omhoog verplaatst
3. De obstakels worden *random* gegenereerd met een rndGen. Door een class moet de afstand tussen de obstakels (pijpleidingen in FB) ook automatisch even groot zijn.
..* Eventueel een paar op 1 window
..* Onderzoeken hoe we de obstakels voorbij kunnen laten gaan (of extra).
4. De collision onderzoeken we door de bird.height te vergelijken met de obstakels hun height.

###### Het is nog *totaal* niet af, een proefconcept. Alle hulp is welkom, idee�n ook.


